import { createRouter, createWebHistory } from 'vue-router'
import Dashboard from '../views/Dashboard.vue'
import AppointmentList from '../views/AppointmentList.vue'
import AddAppointment from '../views/AddAppointment.vue'

const routes = [
  {
    path: "/",
    component: Dashboard,
    meta: { title: 'Dashboard' }
  },
  {
    path: "/create",
    component: AddAppointment,
    meta: { title: 'Create New Appointment' }
  },
  {
    path: "/edit/:item",
    component: AddAppointment,
    props: true,
    meta: { title: 'Edit Appointment' }
  },
  {
    path: "/appointmentList",
    component: AppointmentList,
    meta: { title: 'Appointment List' }
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
